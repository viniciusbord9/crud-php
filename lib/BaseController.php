<?php

class BaseController{
    
    private $_content;
    protected $_model;
    protected $_data;
    protected $_messages;
    
    public function renderView($view, $data = NULL){
        $this->_data = $data;
        $this->_content = $view;
    }
    
    public function getPost(){
       return $_POST;
    }
    
    public function getQuery($key){
        return $_GET[$key];
    }
    
    public function getContent(){
        return $this->_content;
    }
    
    public function getMessages(){
        return $this->_messages;
    }
    
    public function addSuccessMessage($message){
        $this->_messages['success'][] = $message;
    }
    
    public function addErrorMessage($message){
        $this->_messages['error'][] = $message;
    }
    
    public function redirect($url){
        $_SESSION["messages"] = $this->_messages;
        header("location:/?url=".$url);
    }
    
    public function getData(){
        return $this->_data;
    }
    
    public function getModel(){
        return $this->_model;
    }
}

