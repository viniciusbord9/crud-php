<?php

class BaseModel {
    protected $_dbHandle;
    protected $_result;
    protected $_table;
    protected $_conn;
 
    function connect($dbhost, $dbuser, $dbpass, $dbname) {
        try{
            $this->_conn = new PDO("mysql:host=$dbhost;dbname=$dbname",$dbuser,$dbpass);
        }  catch (Exception $ex){
            echo $ex->getMessage();
        }
    }
 
    function disconnect() {
       $_conn = NULL;
    }
     
    function selectAll() {
        $query = 'select * from `'.$this->_table.'`';
        $q = $this->_conn->prepare($query);
        $result = $q->execute();
        if(!$result){
            throw new Exception("Query Error");
        }else{
            return $q->fetchAll();
        }
    }
     
    function select($id) {
        if(is_int($id)){
            $query = 'select * from `'.$this->_table.'` where `id` = \''.$id.'\'';
            $q = $this->_conn->prepare($query);
            $result = $q->execute();
            if(!$result){
                throw new Exception("Query Error");
            }else{
                return $q->fetch(PDO::FETCH_ASSOC);
            }
        }else{
            throw new Exception("Parameter not to be a integer");
        }
    }
 
    
 
    /** Get number of rows **/
    function getNumRows() {
        return mysql_num_rows($this->_result);
    }
 
    /** Free resources allocated by a query **/
 
    function freeResult() {
        mysql_free_result($this->_result);
    }
 
    /** Get error string **/
 
    function getError() {
        return mysql_error($this->_dbHandle);
    }
}