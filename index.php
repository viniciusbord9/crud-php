<?php
include("config/database.config.php");
include("lib/BaseController.php");
include("lib/BaseModel.php");
include("Application.php");


try{
    session_start();
    Application::_initSession();
    Application::_initModels();
    Application::_initController();
    $controller = Application::getCurrentController();
    $action = Application::getCurrentAction();
    $controller->$action();
    $content = $controller->getContent();
    $messages = $controller->getMessages();
    $data = $controller->getData();
    $clean_messages = false;
    include("application/layout/layout.php");
    if($clean_messages){
        Application::_cleanSession($messages);
    }
} catch (Exception $ex) {
    echo $ex->getMessage();
}