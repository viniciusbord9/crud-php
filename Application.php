<?php
class Application{
    private static $content;
    private static $current_controller;
    private static $current_action;
    private static $current_model;
    
    static function _initModels(){
       $models = include "config/models.config.php";
       foreach ($models as $key => $model) {
           require_once $model . '.php';
       }
    }
    
    
    static function _initController(){
        $uri = str_replace("?", "", $_SERVER["REQUEST_URI"]);
        if(key_exists('url', $_GET)){
            $url_array = explode("/", $_GET["url"]);
            if(!is_array($url_array) && count($url_array) < 2){
                 throw new Exception('Request need of the at least one controller');
            }
            $controller_name = ucfirst($url_array[0]);
            Application::$current_action = $url_array[1];
            $class_name = ucfirst($controller_name."Controller");
            $path_to_controller = "application/controllers/".$controller_name."Controller.php";
            if(file_exists($path_to_controller)){
                require_once $path_to_controller;
                $controller = new $class_name();
                if(count($_SESSION['messages']) > 0){
                    $messages = $_SESSION['messages'];
                    foreach($messages as $key => $value){
                        if($key == "success"){
                            $controller->addSuccessMessage($value);
                        }else{
                            $controller->addErrorMessage($value);
                        }
                    }
                }
                $actions = get_class_methods(get_class($controller));
                Application::$current_controller = $controller;
                if(!in_array(Application::$current_action, array_values($actions))){
                    throw new Exception('Action not found');
                }
            }else{
                //verifying if there is a file to controller
                if(file_exists($path_to_controller)){
                    throw new Exception('Controller not found');
                }
            }
        }else{
            header("location:/?url=index/index");
        }
    }
        
    static function getCurrentController(){
        return Application::$current_controller;
    }
    
    static function getCurrentAction(){
        return Application::$current_action;
    }
    
    static function _initSession(){
        if(!key_exists('messages', $_SESSION)){
            $_SESSION['messages'] = array();
        }
    }
    
    static function _cleanSession($messages){
        $_SESSION['messages'] = array();
    }
    
}
