# Instructions

1) Get the code: 
```sh
$ git clone git@bitbucket.org:viniciusbord9/crud-php.git
```
2) Create the database by script:
```
 DOCS/crud-php.sql
```
3) Config connection SGBD creating the
```
 config/database.config.php 
```
-- use config/database.config.dist 
 
4) Config your server :)