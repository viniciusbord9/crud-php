<?php

class Users extends BaseModel{
    
    protected $id;
    
    protected $password;
    
    protected $email;
       
    function __construct() {
        $this->connect(DB_HOST,DB_USER,DB_PASSWORD,DB_NAME);
        $this->_model = get_class($this);
        $this->_table = strtolower($this->_model);
    }
    
    public function setId($id){
        $this->id = $id;
    }
    
    public function setEmail($email){
        $this->email = $email;
    }
    
    public function setPassword($password){
        $this->password = md5($password);
    }
    
    public function getId(){
        return $this->id;
    }
    
    public function getEmail(){
        return $this->email;
    }
    
    public function getPassword(){
        return $this->password;
    }
    
    public function find($id){
        $result = parent::select($id);
        if($result){
            $this->id = $result["id"];
            $this->email = $result["email"];
            $this->password = $result["password"];
        }
    }
       
    public function save(){
        $sql = "INSERT INTO users (email,password) VALUES (:email,:password)";
        $q = $this->_conn->prepare($sql);
        $result = $q->execute(array(':email'=>$this->email,':password'=>$this->password));
        if(!$result){
            throw new Exception("Não foi possível salvar Usuário");
        }
    }
    
    public function update(){
       $sql = "UPDATE users SET email=:email, password=:password WHERE id=:id;";
       $q = $this->_conn->prepare($sql);
       $result = $q->execute(array(':email' => $this->email,':password'=>md5($this->password),':id'=>$this->id));
       if(!$result){
           throw new Exception("Não foi possível editar Usuário");
       }
       
    }
    
    public function delete(){
        $sql = "DELETE FROM users WHERE id=:id";
        $q = $this->_conn->prepare($sql);
        $result = $q->execute(array(':id'=>$this->id));
        if(!$result){
            throw new Exception("Não foi possível deletar Usuário");
        }
    }
}
