<?php

class IndexController extends BaseController{
    function __construct() {
        $this->_model = new Users();
        $this->_messages = array();
    }
    
    public function index(){
        $view = "application/views/index/index.php";
        $this->renderView($view);
    }
  
    public function add(){
       $post = $this->getPost();
       try{
           $this->_model->setEmail($post['email']);
           $this->_model->setPassword($post['password']);
           if(isset($post['id'])){
               $this->_model->setId(intval($post['id']));
               $this->addSuccessMessage("Usuário editado com sucesso");
               $this->_model->update();
           }else{
               $this->addSuccessMessage("Usuário inserido com sucesso");
                $this->_model->save();
           }
           
           $this->redirect("index/users");
       } catch (Exception $ex) {
           $this->addErrorMessage("Não foi possível inserir usuário");
           $this->redirect("index/index");
       }
    }
    
    public function users(){
        $data = $this->_model->selectAll();
        $view = "application/views/index/list.php";
        $this->renderView($view, $data);
    }
    
    public function edit(){
        $id = $this->getQuery('id');  
        $data = $this->_model->select(intval($id));
        $view = "application/views/index/index.php";
        $this->renderView($view, $data);
    }
    
    public function delete(){
        $post = $this->getPost();
        $this->_model->find(intval($post['id']));
        if(!is_null($this->_model->getId()) && $this->_model->getEmail() == $post["email"] && md5($post["password"]) == $this->_model->getPassword()){
            $this->_model->delete();
            $this->addSuccessMessage("Usuário excluído com sucesso");
            $this->redirect("index/users");
        }else{
            $this->addErrorMessage("Não foi possível excluir usuário, verifique se você digitou a senha antes da operação");
            $url = "index/edit&id=".$post['id'];
            $this->redirect($url);
        }
        
    }
}

