<div class="container">
    <?php if(count($messages) >0  && is_array($messages)): ?>
    <?php foreach($messages as $key => $items): ?>
       <?php if($key == "success"): ?>
            <?php foreach($items as $k => $item): ?>
                <div class="alert alert-success" role="alert"><?php echo $item[0] ?></div>
            <?php endforeach; ?>
       <?php else: ?>
            <?php foreach($items as $k => $item): ?>
                <div class="alert alert-danger" role="alert"><?php echo $item[0] ?></div>
            <?php endforeach; ?>
       <?php endif ?>
       <?php $clean_messages = true; ?>
    <?php endforeach; ?>
    <?php endif; ?>
    <form class="form-signin" id="userform" action="?url=index/add" method="POST">
        <h2 class="form-signin-heading">Cadastre-se</h2>
        <?php if($data != NULL): ?>
        <input type="hidden" name="id" value="<?php echo ($data != NULL) ? $data["id"] : ''?>">
        <?php endif; ?>
        <input type="text" name="email" class="input-block-level" value="<?php echo ($data != NULL) ? $data["email"] : ''?>" placeholder="Endereço de E-mail">
        <input type="password" name="password" class="input-block-level" placeholder="Senha">
        <button class="btn btn-large btn-primary" type="submit" value="save">Salvar</button>
        <button onclick="window.location='?url=index/index';" class="btn btn-large btn-primary" type="reset">Incluir Novo</button>
        <?php if($data != NULL): ?>
            <button type="button" onclick="deleteAction('<?php echo $data["id"] ?>');" class="btn btn-large btn-primary">Excluir</button>
        <?php endif; ?>  
        <div class="row">
            <div class="span4 offset3">
                <a href="?url=index/users">Visualizar Usuários</a>
            </div>
        </div>
    </form>
    
    
    

</div>

