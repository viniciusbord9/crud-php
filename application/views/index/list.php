<div class="container">
<?php if(count($messages) >0  && is_array($messages)): ?>
 <?php foreach($messages as $key => $items): ?>
    <?php if($key == "success"): ?>
         <?php foreach($items as $item): ?>
             <div class="alert alert-success" role="alert"><?php echo $item[0] ?></div>
         <?php endforeach; ?>
    <?php else: ?>
         <?php foreach($items as $item): ?>
             <?php var_dump($item) ?>
             <div class="alert alert-danger" role="alert"><?php echo $item[0] ?></div>
         <?php endforeach; ?>
    <?php endif ?>
    <?php $clean_messages = true; ?>
 <?php endforeach; ?>
 <?php endif; ?>
<div class="row">             
    <div class="span3">
        <h2>Usuários</h2>
    </div>
    <div class="span3 offset6">
        <a class="btn btn-large btn-primary" href="?url=index/index">Novo Usuario</a>
    </div>
</div>
<table class="table table-striped">
      <thead>
        <tr>
          <th>#</th>
          <th>Email</th>
          <th>Ações</th>
        </tr>
      </thead>
      <tbody>
        <?php foreach($data as $user): ?>
        <tr>
          <th scope="row"><?php echo $user["id"] ?></th>
          <td><?php echo $user["email"] ?></td>
          <th><a href="<?php echo '?url=index/edit&id='.$user["id"]; ?>">Editar</a></th>
        </tr>
        <?php endforeach; ?>
      </tbody>
    </table>
</div>